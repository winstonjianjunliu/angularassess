import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RenderComponent } from './render/render.component';
import { SwformComponent } from './swform/swform.component';
import { InfoService } from './info.service';

@NgModule({
  declarations: [
    AppComponent,
    RenderComponent,
    SwformComponent
  ],
  imports: [
    BrowserModule, FormsModule,  HttpClientModule
  ],
  providers: [InfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
