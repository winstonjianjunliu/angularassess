import { Component, OnInit, Input } from '@angular/core';
import { Info } from '../models/info';

@Component({
  selector: 'app-render',
  templateUrl: './render.component.html',
  styleUrls: ['./render.component.css']
})
export class RenderComponent implements OnInit {

  @Input() result:any

  infoModel = new Info()

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {

    console.log(this.result.name)
    console.log(this.result.type)

    this.infoModel.name = this.result.name
    this.infoModel.type = this.result.type
  }

}
