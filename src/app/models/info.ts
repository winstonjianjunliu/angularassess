//declare a class
export class Info{ 
    //class properties
    name: string = "Winston"
    type: string = "handsome"

    //we can declare an optional constructor
    constructor(){}   

}