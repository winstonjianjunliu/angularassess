import { Component } from '@angular/core';
import { Info } from './models/info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Star War API';

  result = new Info()

  submitEventHandler(data) {
    this.result = data
  }
}
