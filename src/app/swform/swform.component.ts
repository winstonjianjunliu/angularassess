import { Component, OnInit, Output } from '@angular/core';
import {NgForm} from '@angular/forms'
import { EventEmitter } from '@angular/core';
import { InfoService } from '../info.service';


@Component({
  selector: 'app-swform',
  templateUrl: './swform.component.html',
  styleUrls: ['./swform.component.css']
})
export class SwformComponent implements OnInit {

  constructor(private infoService: InfoService) { }

  urlOptions = [
    { category: 'people' , valueUrl: 'https://swapi.co/api/people/'},
    { category: 'planets', valueUrl: 'https://swapi.co/api/planets/'},
    { category: 'films', valueUrl: 'https://swapi.co/api/films/'},
    { category: 'species', valueUrl: 'https://swapi.co/api/species/'},
    { category: 'vehicles', valueUrl: 'https://swapi.co/api/vehicles/'},
    { category: 'starships', valueUrl: 'https://swapi.co/api/starships/'}
  ]

  infoUrl = 'https://swapi.co/api/'

  selectedCategory
  selectedNumber
  selectedUrl = ''
  result

  @Output() submitEvent = new EventEmitter()

  ngOnInit() {
    this.selectedCategory = this.urlOptions[0].category
    this.selectedNumber = 1
  }


  public onSubmit(form: NgForm) {
    this.selectedCategory = form.value.category
    this.selectedNumber = form.value.number

    console.log(this.selectedCategory)

    this.infoService.getInfo(this.infoUrl + this.selectedCategory + "/" + this.selectedNumber).subscribe((result) => {

      result.type = this.selectedCategory

      this.result = result
       
      console.log(this.result)

      this.submitEvent.emit(this.result)
    })

    

  }

}